#ifndef HEADER_H
#define HEADER_H

#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

//������������� ������������
enum Key {
	APHORISM,
	PROVERB,
	RIDDLE
};

//��������� "������������"
struct Storehouse {
	Key K; //������������� ������������
	string Content; //���������� �����������
	int Estimation; //������������ ������ ������������
};

//���������
struct Container {
	Container* Next, *Prev; //��������� �� ����. � ����. �������� ����������
	Storehouse* Cont; //��������� �� �������������
	int Len; //����� ��������� ����������
};

//��������� "�������"
struct Aphorism {
	Key K; //������������� ������������
	string Content; //���������� ��������
	int Estimation; //������������ ������ ������������
	string Author; //������ ��������
};

//��������� "���������"
struct Proverb {
	Key K; //������������� ������������
	string Content; //���������� ���������
	int Estimation; //������������ ������ ������������
	string Country; //������ ���������
};

//��������� "�������"
struct Riddle {
	Key K; //������������� ������������
	string Content; //���������� �������
	int Estimation; //������������ ������ ������������
	string Answer; //�����
};

//������� ������������� ����������
void Init_Container(Container* Head);

//������� ����� ��������� � ���������
void In_Container(Container* Head, ifstream& ifst);

//������� ������ �������� ����������
void Out_Container(Container* Head, ofstream& ofst);

//������� ������� ����������
void Clear_Container(Container* Head);

//������� ���������� ����������
void Sort(Container* Head);

//������� ������ ������ ���������
void Out_Only_Aphorism(Container* Head, ofstream& ofst);

//������� ����� ������������
Storehouse* In_Storehouse(ifstream& ifst);

//������� �������� ����� ������ ���������� � ���������� ������������
int Amount(Storehouse* St);

//������� ��������� ����� ������ ���������� ���� ������������
bool Compare(Storehouse* First, Storehouse* Second);

//������� ������ ������������
void Out_Storehouse(Storehouse* St, ofstream& ofst);

//������� ����� ��������
void* In_Aphorism(ifstream& ifst);

//������� ������ ��������
void Out_Aphorism(string Content, int Estimation, Aphorism* A, ofstream& ofst);

//������� ����� ���������
void* In_Proverb(ifstream& ifst);

//������� ������ ���������
void Out_Proverb(string Content, int Estimation, Proverb* P, ofstream& ofst);

//������� ����� �������
void* In_Riddle(ifstream& ifst);

//������� ������ �������
void Out_Riddle(string Content, int Estimation, Riddle* R, ofstream& ofst);

#endif // HEADER_H