#include "Header.h"

void Init_Container(Container* Head) {
	Head->Cont = NULL;
	Head->Len = 0;
	Head->Next = Head->Prev = Head;
}

void In_Container(Container* Head, ifstream& ifst) {
	Container* Inserted, *Curr;
	Curr = Inserted = NULL;
	int Len = 0;

	while (!ifst.eof()) {
		Inserted = new Container();
		if ((Inserted->Cont = In_Storehouse(ifst))) {
			if (!Len) {
				Head->Cont = Inserted->Cont;
			}
			else {
				Curr = Head->Next;
				Head->Next = Inserted;
				Inserted->Next = Curr;
				Inserted->Prev = Head;
				Curr->Prev = Inserted;
				Head = Inserted;
			}

			Len++;
		}
	}

	Head = Head->Next;

	//���������� ����� ��������� ���������� � ������ ����
	for (int i = 0; i < Len; i++) {
		Head->Len = Len;
		Head = Head->Next;
	}
}

void Out_Container(Container* Head, ofstream& ofst) {
	ofst << "Container contains " << Head->Len
		<< " elements." << endl << endl;

	Container* Temp = Head;

	for (int i = 0; i < Head->Len; i++) {
		ofst << i << ": ";
		Out_Storehouse(Temp->Cont, ofst);
		ofst << "Amount of punctuation marks in the content of storehouse = " <<
			Amount(Temp->Cont) << endl << endl;
		Temp = Temp->Next;
	}
}

void Clear_Container(Container* Head) {
	for (int i = 0; i < Head->Len; i++) {
		free(Head->Cont); //������� ������, ����������� ���������� ���������� � ��������
		Head->Len = 0; //��������� ����������� ���������� ��� ������ ��� ���� 0
		Head = Head->Next; //��������� � ����. �������� ����������
	}
}

void Sort(Container* Head) {
	if (Head->Len > 1) {
		Container* First = Head;
		Container* Second = Head->Next;

		Container* Temp = new Container;

		for (int i = 0; i < Head->Len - 1; i++) {
			for (int j = 0; j < Head->Len - i - 1; j++) {
				if (Compare(First->Cont, Second->Cont)) {
					Temp->Cont = First->Cont;
					First->Cont = Second->Cont;
					Second->Cont = Temp->Cont;
				}

				Second = Second->Next;
			}

			First = First->Next;
			Second = First->Next;
		}
	}
}

void Out_Only_Aphorism(Container* Head, ofstream& ofst) {
	ofst << "Only Aphorisms." << endl << endl;

	Container* Temp = Head;

	for (int i = 0; i < Head->Len; i++) {
		if (Temp->Cont->K == APHORISM) {
			ofst << i << ": ";
			Out_Storehouse(Temp->Cont, ofst);
			ofst << endl;
		}

		Temp = Temp->Next;
	}
}

Storehouse* In_Storehouse(ifstream& ifst) {
	Storehouse* St = new Storehouse();
	string Temp = "";

	getline(ifst, Temp);

	int K = atoi(Temp.c_str());

	if (K == 1) { //���� K == 1, �� ��� ��������
		St = (Storehouse*)In_Aphorism(ifst);
		St->K = APHORISM;
	}
	else if (K == 2) { //���� K == 2, �� ��� ���������
		St = (Storehouse*)In_Proverb(ifst);
		St->K = PROVERB;
	}
	else if (K == 3) { //���� K == 3, �� ��� �������
		St = (Storehouse*)In_Riddle(ifst);
		St->K = RIDDLE;
	}
	else {
		return 0;
	}

	return St;
}

void Out_Storehouse(Storehouse* St, ofstream& ofst) {
	if (St->K == APHORISM) {
		Out_Aphorism(St->Content, St->Estimation, (Aphorism*)St, ofst);
	}
	else if (St->K == PROVERB) {
		Out_Proverb(St->Content, St->Estimation, (Proverb*)St, ofst);
	}
	else if (St->K == RIDDLE) {
		Out_Riddle(St->Content, St->Estimation, (Riddle*)St, ofst);
	}
	else {
		ofst << "Incorrect element!" << endl;
	}
}

int Amount(Storehouse* St) {
	if (St->K == APHORISM || St->K == PROVERB || St->K == RIDDLE) {
		string Alph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789";
		int Amount = 0;

		for (int i = 0; i < St->Content.size(); i++) {
			if (Alph.find(St->Content[i]) == -1) {
				Amount++;
			}
		}

		return Amount;
	}
	else {
		return -1;
	}
}

bool Compare(Storehouse* First, Storehouse* Second) {
	return Amount(First) > Amount(Second);

}

void* In_Aphorism(ifstream& ifst) {
	Aphorism* A = new Aphorism();

	getline(ifst, A->Content);
	getline(ifst, A->Author);

	string Temp = "";

	getline(ifst, Temp);

	A->Estimation = atoi(Temp.c_str());

	return A;
}

void Out_Aphorism(string Content, int Estimation, Aphorism* A, ofstream& ofst) {
	ofst << "It's an Aphorism: " << Content << endl;
	ofst << "Aphorism's author is: " << A->Author << endl;
	ofst << "Subjective estimation of the adage: " << Estimation << endl;
}

void* In_Proverb(ifstream& ifst) {
	Proverb* P = new Proverb();

	getline(ifst, P->Content);
	getline(ifst, P->Country);

	string Temp = "";

	getline(ifst, Temp);

	P->Estimation = atoi(Temp.c_str());

	return P;
}

void Out_Proverb(string Content, int Estimation, Proverb* P, ofstream& ofst) {
	ofst << "It's a Proverb: " << Content << endl;
	ofst << "Proverbs's country is: " << P->Country << endl;
	ofst << "Subjective estimation of the adage: " << Estimation << endl;
}

void* In_Riddle(ifstream& ifst) {
	Riddle* R = new Riddle();

	getline(ifst, R->Content);
	getline(ifst, R->Answer);

	string Temp = "";

	getline(ifst, Temp);

	R->Estimation = atoi(Temp.c_str());

	return R;
}

void Out_Riddle(string Content, int Estimation, Riddle* R, ofstream& ofst) {
	ofst << "It's a Riddle: " << Content << endl;
	ofst << "Riddle's answer is: " << R->Answer << endl;
	ofst << "Subjective estimation of the adage: " << Estimation << endl;
}